@echo off
echo off
echo -----------------------------
echo Installation suite desclicks
echo .
echo version 20/09/2013
echo auteur : rogue-spectre
echo .
echo -----------------------------
REM get path of script 
SET mypath=%~dp0
SET begin=%mypath:~0,-1%
cd %begin%
REM add PATH of wget executable
setlocal
set PATH=%PATH%;%begin%\wget\bin
echo .
color A
echo  [ Choix Utilisateurs ]
set WRITE_DESCLICKS_INFOS=t
set ANTIVIRUS=1
set /p WRITE_DESCLICKS_INFOS="Inscrire les donnes desclicks dans les donnes systeme ? [t|f] : "
set /p ANTIVIRUS="Choix Anti-Virus : 1- Avast, 2- Avira, 0- Aucun : [1|2|0] :"
echo "%WRITE_DESCLICKS_INFOS% %ANTIVIRUS%"
echo ----------------------------------------------------
echo  Si vous avez choisi l installation d un antivirus, 
echo  l installateur de celui ci sera lance
echo  en fin de script, dans environ 15 min...           
echo ----------------------------------------------------

echo [ Sauvegarde du registre ]
regedit /e c:\bck_before_desclicks_silent_install.reg

echo [ Determination de l'os win ]
call:winos
echo %WIN_OS%
echo %WIN_ARCH%


if "%WRITE_DESCLICKS_INFOS%"=="t" (
	call:write_sysinfos
)

echo [ Telechargement des logiciels ]
SET OPTANTIVIRUS=--antivirus None
if "%ANTIVIRUS%" == "1" SET OPTANTIVIRUS=--antivirus Avast
if "%ANTIVIRUS%" == "2" SET OPTANTIVIRUS=--antivirus Avira

REM echo currentLinks.exe %OPTANTIVIRUS%
REM if exist "currentLinks.exe" currentLinks.exe %OPTANTIVIRUS%

if "%ANTIVIRUS%" == "1" (
REM AVAST
    wget files.avast.com/iavs9x/avast_free_antivirus_setup.exe -O avast_intaller.exe
    start /wait avast_installer.exe /nosounds /nodesktopicon
)
if "%ANTIVIRUS%" == "2" (
REM AVIRA
    wget http://personal.avira-update.com/package/wks_avira13/win32/fr/pecl/avira_free_antivirus_fr.exe -O avira.exe
    avira.exe
)
REM
REM Internet ...................................................................
REM

REM Firefox
wget --no-check-certificate http://wiki.desclicks.net/var/ressources/scripts/get_versions/current_firefox_link.php                 -O firefox_link
wget -i firefox_link -O firefox_installer.exe

REM Flash
wget http://download.macromedia.com/pub/flashplayer/current/support/install_flash_player.exe

REM
REM Bureautique ................................................................
REM

REM 7-ZIP
wget --no-check-certificate http://wiki.desclicks.net/var/ressources/scripts/get_versions/current_7zip_link.php                    -O 7zip_link
wget -i 7zip_link -O 7zip_installer.exe

REM LibreOffice
wget --no-check-certificate http://wiki.desclicks.net/var/ressources/scripts/get_versions/current_stable_libreoffice_link.php      -O libreoffice_link
wget -i libreoffice_link -O libreoffice_installer.msi

REM LibreOffice help
wget --no-check-certificate http://wiki.desclicks.net/var/ressources/scripts/get_versions/current_stable_libreofficehelp_link.php  -O libreofficehelp_link
wget -i libreofficehelp_link -O libreofficehelp_installer.msi -O libreofficehelp_installer.msi

REM Sumatra PDF
wget --no-check-certificate http://wiki.desclicks.net/var/ressources/scripts/get_versions/current_SumatraPDF_link.php              -O SumatraPDF_link
wget -i SumatraPDF_link -O sumatraPDF_installer.exe

REM Visionneuse powerpoint
wget http://download.microsoft.com/download/6/B/4/6B4CA627-C0CD-4B27-BE31-0B87AE10F46B/PowerPointViewer.exe

REM
REM Multimedia ..................................................................
REM 

REM VLC
wget --no-check-certificate http://wiki.desclicks.net/var/ressources/scripts/get_versions/current_vlc_link.php                     -O vlc_link
wget -i vlc_link -O vlc_installer.exe

REM InfraRecorder
wget http://sourceforge.net/projects/infrarecorder/files/latest/download?source=dlp -O InfraRecorder.exe
 
echo .
echo [ INSTALLATION ]
echo .

REM Internet
firefox_installer.exe -ms -ira
install_flash_player.exe -install

REM Bureautique
7zip_installer.exe /S
msiexec /i libreoffice_installer.msi ALLUSER=1 SELECT_WORD=1 SELECT_EXCEL=1 SELECT_POWERPOINT=1 USERNAME="" COMPANYNAME="" /quiet
msiexec /i libreofficehelp_installer /quiet
sumatraPDF_installer.exe /S
PowerPointViewer.exe /quiet /norestart

REM Multimedia
vlc_installer.exe -y /q /r:n /S
InfraRecorder.exe /S

echo .
echo [ SUPPRESSION ] raccourcis desktop
echo .
REM del "%Public%\Desktop\LibreOffice*.lnk"
REM del "%Public%\Desktop\Mozilla Firefox.lnk"

del "%ALLUSERSPROFILE%\Desktop\CCleaner.lnk"
del "%Public%\Desktop\CCleaner.lnk"
del "%HOMEPATH%\Desktop\CCleaner.lnk"

del "%Public%\Desktop\InfraRecorder.lnk"
del "%HOMEPATH%\Desktop\InfraRecorder.lnk"

del "%Public%\Desktop\VLC*.lnk"
del "%HOMEPATH%\Desktop\VLC*.lnk"

del "%Public%\Desktop\*avast*.lnk"
del "%Public%\Desktop\*Avast*.lnk"
del "%HOMEPATH%\Desktop\Avast*.lnk"
del "%HOMEPATH%\Desktop\avast*.lnk"


echo [ Wallpaper ]
call:setBackground

echo =====================================
echo =====================================
echo  mise a jour des fichiers geres par 7zip
echo =====================================
echo =====================================


echo [ Sauvegarde du registre ]
regedit /e c:\bck_desclicks_silent_install_avant_reglages_7zip.reg
SETLOCAL

if exist "%PROGRAMFILES%\7-Zip" SET PG=%PROGRAMFILES%
if exist "%PROGRAMFILES(x86)%\7-Zip" SET PG=%PROGRAMFILES(x86)%

SET SC=HKLM\SOFTWARE\Classes
SET Extn=001 7z arj bz2 bzip2 cab chm cpio deb gz lzh lzma nsis rar rpm tar z


FOR %%j IN (%Extn%) DO (
        REG ADD %SC%\.%%j /VE /D "7-Zip.%%j" /F
        REG ADD %SC%\7-Zip.%%j /VE /D "7z Archive" /F

        REM REG ADD %SC%\7-Zip.%%j\DefaultIcon /VE /D "%PROGRAMFILES%\7-Zip\Formats\%%j.dll" /F
        REG ADD %SC%\7-Zip.%%j\shell\open\command /VE /D "%PROGRAMFILES(x86)%\7-Zip\7zFM.exe %%1" /F

)

echo "COPIE ICONES 7zip"
COPY /V /Y zip.ico "%PG%\7-Zip"
COPY /V /Y generic-archive.ico "%PG%\7-Zip"

SET MYICO=TRUE
if not exist "%PG%\7-Zip\zip.ico" SET MYICO=FALSE
if not exist "%PG%\7-Zip\generic-archive.ico" SET MYICO=FALSE

if  "%MYICO%"=="TRUE"  (
    echo "====== set icon for 7zip ======="
    REG ADD %SC%\7-Zip.zip\DefaultIcon /VE /D "%PG%\7-Zip\zip.ico" /F
    SET Extn=001 7z arj bz2 bzip2 cab chm cpio deb gz lzh lzma nsis rar rpm tar z
    FOR %%j IN (%Extn%) DO (
	REG ADD %SC%\7-Zip.%%j\DefaultIcon /VE /D "%PG%\7-Zip\generic-archive.ico" /F
    )
    REG ADD %SC%\7-Zip.zip\DefaultIcon /VE /D "%PG%\7-Zip\zip.ico" /F
) else ( 
    REG ADD %SC%\7-Zip.%%j\DefaultIcon /VE /D "%PG%\7-Zip\7z.dll" /F
)
ENDLOCAL


echo.
echo =========================================
echo .
echo  FIN DE L'INSTALLATION
echo .
echo   - Si tout est ok pour 7zip notamment
echo     supprimez manuellement les deux .reg 
echo     de sauvegarde places a la racine C:\
echo .
echo   - Activez les extensions de firefox
echo .
echo   - Epinglez Firefox et libreOffice 
echo     dans la barre des taches
echo .
echo   - Supprimez IE de la barre des taches et
echo   des favoris du menu
echo .
echo =========================================
echo .
set SUPPR_DESCLICKS=t
set /p SUPPR_DESCLICKS="Supprimer le dossier et l'archive suite-desclicks ? (t/f)"
echo .
if "%SUPPR_DESCLICKS%"=="t" (
    cd ..
    del suite-desclicks.zip
    rd  suite-desclicks /S /Q

)

echo "on tue explorer pour mieux le relancer, fermez la fenetre ;)"
taskkill /im explorer.exe /f && timeout 2 && explorer
pause

goto:eof


REM =====================================
REM =====================================
REM fonctions utiles au scripts
REM =====================================
REM =====================================


REM =====================================
REM setBackground
REM =====================================
:setBackground
echo %WIN_OS% | find /i "seven"
if %ERRORLEVEL% == 0  goto setBackground_seven_vista

echo %WIN_OS% | find /i "vista"
if %ERRORLEVEL% == 0  goto setBackground_seven_vista

echo %WIN_OS% | find /i "XP"
if %ERRORLEVEL% == 0  goto setBackground_xp

:setBackground_seven_vista
SET DEFAULT_WALLPAPER_DESCLICKS=TranscodedWallpaper.jpg
if exist "%DEFAULT_WALLPAPER_DESCLICKS%" (
    COPY /V /Y "%DEFAULT_WALLPAPER_DESCLICKS%" "%WINDIR%\Web\Wallpaper"
    COPY /V /Y "%DEFAULT_WALLPAPER_DESCLICKS%" "%APPDATA%\Microsoft\Windows\Themes"
)

if exist "%APPDATA%\Microsoft\Windows\Themes" (
    reg add "HKCU\Software\Microsoft\Internet Explorer\Desktop\General" /v WallpaperSource /d "%APPDATA%\Microsoft\Windows\Themes%DEFAULT_WALLPAPER_DESCLICKS%" /F
)
goto exit

:setBackground_xp
SET DEFAULT_WALLPAPER_DESCLICKS=default-desclicks-wallpaper.bmp
if exist "%DEFAULT_WALLPAPER_DESCLICKS%" (
    COPY /V /Y "%DEFAULT_WALLPAPER_DESCLICKS%" "%WINDIR%\Web\Wallpaper"
)

if exist "%WINDIR%\Web\Wallpaper\%DEFAULT_WALLPAPER_DESCLICKS%" (
    Rem modification du fond d ecran
    reg add "HKCU\Control Panel\Desktop" /v Wallpaper /d "%WINDIR%\Web\Wallpaper\%DEFAULT_WALLPAPER_DESCLICKS%" /F
    Rem centrer le fond d ecran
    reg add "HKCU\Control Panel\Desktop" /v TileWallpaper /d "0" /F
    RUNDLL32.EXE USER32.DLL,UpdatePerUserSystemParameters ,1 ,True
)
goto exit

:exit
goto eof



REM =====================================
REM determine windows version and arch
REM =====================================
:winos
REM source http://malektips.com/xp_dos_0025p.html

if defined ProgramFiles(x86) (
set WIN_ARCH=64
) else (
set WIN_ARCH=32
)

ver | find "2003" > nul
if %ERRORLEVEL% == 0 goto ver_2003

ver | find "XP" > nul
if %ERRORLEVEL% == 0 goto ver_xp

ver | find "2000" > nul
if %ERRORLEVEL% == 0 goto ver_2000

ver | find  "NT" > nul
if %ERRORLEVEL% == 0 goto ver_nt

ver | find /i "version 6.1" > nul
if %ERRORLEVEL% == 0 goto ver_7

ver | find "version 6.0" > nul
if %ERRORLEVEL% == 0 goto ver_vista

goto warnthenexit

:ver_7
set WIN_OS=seven
goto exit

:ver_vista
set WIN_OS=vista
goto exit

:ver_xp
set WIN_OS=xp
goto exit

:ver_2000
set WIN_OS=2000
goto exit

:ver_nt
set WIN_OS=NT
goto exit

:warnthenexit
echo Machine undetermined.

:exit
goto eof


REM =====================================
REM write sysinfos
REM =====================================
:write_sysinfos
echo [ Inscription des donnes Desclicks ]

echo %WIN_OS% | find /i "seven"
if %ERRORLEVEL% == 0  goto write_sysinfos_seven_vista

echo %WIN_OS% | find /i "vista"
if %ERRORLEVEL% == 0  goto write_sysinfos_seven_vista

echo %WIN_OS% | find /i "XP"
if %ERRORLEVEL% == 0  goto write_sysinfos_xp

echo "sysinfo can not be written"

goto exit

:write_sysinfos_seven_vista
echo "writing oeminfos for seven or vista"

SET SC=HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\OEMInformation

REG ADD %SC% /V "Manufacturer"  /t REG_SZ /D "Desclicks - l'informatique solidaire. 3 rue St Paul, 67300 Schiltigheim" /F
REG ADD %SC% /V "SupportPhone"  /t REG_SZ /D "03 88 83 64 10    -    Mail : contact@desclicks.net    -    Site : www.desclicks.net" /F
REG ADD %SC% /V "Logo"          /t REG_SZ /D "C:\Windows\System32\oemlogo.bmp" /F

REM REG ADD %SC% /V "SupportHours"  /t REG_SZ /D "www.desclicks.net/contact" /F
REM REG ADD %SC% /V "SupportURL"    /t REG_SZ /D "www.desclicks.net" /F

COPY /V /Y oemlogo.bmp   "%windir%\system32"
goto exit

:write_sysinfos_xp
COPY /V /Y oeminfos.ini  "%windir%\system32"
COPY /V /Y oemlogo.bmp   "%windir%\system32"
goto exit


:exit
goto eof

REM =====================================
REM install antivirus
REM =====================================
:install_antivirus
if "%ANTIVIRUS%" == "1" start /wait Avast.exe /nosounds /nodesktopicon
if "%ANTIVIRUS%" == "2" start /wait Avira.exe

:exit
:goto eof

:eof