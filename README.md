auteur  : rogue-spectre
version : 12/12/2013

=======================================

Utilisation :

=======================================

Le script principal à lancer 

> post-install.bat

Les droits administrateurs sont demandes lors du lancement du script, 
Si plantage executez

>ressources\suite-desclicsk-win.bat 

en admin


=======================================

Ce qui va être fait :

=======================================

Une sauvegarde du registre enregistree a la racine 

Inscription au choix, des données desclicks dans les données systèmes :
    - adresse         : 3 rue St Paul 67300 Schiltigheim
    - mail de contact : contact@desclicks.net
    - site web        : www.desclicks.net
    - telephone       : 03 88 83 64 10
    + logo.bmp
    

Téléchargement et installation silencieuse des programmes suivants :
    '7zip'             
    'LibreOffice'     
    'SumatraPDF'      
    'PowerPointViewer' 
    'VLC'            
    'InfraRecorder'   
    'Firefox'
    'FlashPlayer' ( pour les navigateurs autre que IE ;)

Installation via interface graphique en fait de script d'un antivirus :        
    'Avast'
ou  'Avira'


Installation d'un fond d'écran 
  default-desclicks-wallpaper.bmp  -  XP
  TranscodeWallpaper               -  vista seven
     
Association des fichiers que 7zip peut gérer + icone générique pour les archives


=======================================

Quelques explications :

=======================================

Le fichier suite-desclicks-win.bat fait appel à un programme currentLinks.exe qui permet de 
télécharger les logiciels. currentLinks.exe est un gel d'un script python réalisé à l'aide 
de cx_Freeze. currentLinks nécessite le  programme wget pour windows, ainsi que les deux 
librairies windows MSVCR100.dll msvcp100.dll. currentLinks peut être utilisé seul également, 
les sources python sont dans le dossier src.

- L'icone d'archive est empruntée à l'ensemble d'icône Faenza. 
- Les fonts d'écrans desclicks utilisent la police libre isl_jupiter.